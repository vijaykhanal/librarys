package com.design.android.tumblr.util;

/**
 * Constants.
 * 
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 *
 */
public class Cons {
	public static final String TAG = "AndroidTumblr";
	
	//public static final String API_VERIFY_CREDENTIAL_URL = "";
	
	public static final boolean ENABLE_DEBUG = true;
}